<?php namespace Bitcraft\Publish;

use Bitcraft\Publish\Models\Deployment;
use Bitcraft\Publish\Models\Platform;
use Illuminate\Support\Facades\Event;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        Event::listen('versions.unpublish', function($id) {
            $page = Page::findOrFail($id);
            $page->handlePublishedPages();
        });

        // create default platform
        if (Platform::all()->count() === 0) {
            $platform = new Platform();
            $platform->title = 'Default';
            $platform->settings_id = 1;
            $platform->domain = 'https://example.com';
            $platform->bucket = 'example-website';
            $platform->save();
        }

        // create default deployment model
        if (Deployment::all()->count() === 0) {
            $deployment = new Deployment();
            $deployment->save();
        }
    }

    public function registerSchedule($schedule)
    {
    }

    public function register()
    {
    }
}
