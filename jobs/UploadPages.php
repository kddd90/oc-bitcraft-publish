<?php namespace Bitcraft\Bitpanda\Jobs;

use Bitcraft\Pagebuilder\Controllers\Pages;
use Bitcraft\Pagebuilder\Models\Page;
use Bitcraft\Publish\Classes\Cloudfront;
use Bitcraft\Publish\Models\PastQueueJob;
use Bitcraft\Publish\Models\Platform;
use Bitcraft\SeoManager\Classes\Robots;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use RainLab\Translate\Models\Locale;
use voku\helper\HtmlMin;

class UploadPages
{
    public function fire($job, $data)
    {
        $this->uploadErrorPage();
        $this->uploadSitemap();
        $this->uploadRobots();
        $locales = array_keys(Locale::listEnabled());
        if (!empty($data) && is_array($data) && array_key_exists('id', $data) && $id = $data['id']) {
            $page = Page::findOrFail($id);
            $platform = $page->platform;
            foreach ($locales as $locale) {
                $page->noFallbackLocale()->lang($locale);
                if (($version = $page->currentModel($locale)) && !empty($version->slug) && $version->published) {
                    if (empty($page->slug)) {
                        continue;
                    }

                    $this->storePage($version, $locale, $platform);
                }
            }
            $this->getRedirectHtml($page);
            $this->createPastJobModel($data);
            Cloudfront::invalidate();
            $job->delete();
            return;
        }

        $pages = Page::all();
        foreach ($pages as $page) {
            if (!$page->hasPublishedVersion || !$page->published) {
                continue;
            }

            $platform = $page->platform;
            foreach ($locales as $locale) {
                $page->noFallbackLocale()->lang($locale);
                if (($version = $page->currentModel($locale)) && !empty($version->slug) && $version->published) {
                    if (empty($page->slug)) {
                        continue;
                    }
                    $this->storePage($version, $locale, $platform);
                }
            }
            $this->getRedirectHtml($page);
        }
        $this->createPastJobModel($data);
        Cloudfront::invalidate();
        $job->delete();
    }

    public function storePage($page, $locale, $platform)
    {
        $slug = $page->slug === '/' ? '' : $page->slug;
        $path = $locale.$slug;
        $request = Request::create("/$path?platform=$platform->id", 'GET');
        $controller = new Pages();
        $content = $controller->show($request, null, $locale);
        $this->uploadToS3($content, $path, $platform);
    }

    public function storeRedirect($content, $slug, $platform)
    {
        $path = $slug === '/' ? 'index' : $slug;
        $this->uploadToS3($content, $path, $platform);
    }

    public function storeSitemap($content, $slug, $platform)
    {
        $this->uploadToS3($content, $slug, $platform, 'text/xml', false);
    }

    public function storeRobots($content, $slug, $platform)
    {
        $this->uploadToS3($content, $slug, $platform, 'text/plain', false);
    }

    public function uploadToS3($content, $path, $platform, $content_type = 'text/html', $minify = true)
    {
        if ($minify) {
            $htmlMin = new HtmlMin();
            $content = $htmlMin->minify($content);
        }
        Storage::disk($platform->bucket)->put($path, $content, [
            'visibility' => 'public',
            'ContentType' => $content_type,
            'mimetype' => $content_type
        ]);
    }

    public function createPastJobModel($data)
    {
        $past_job = new PastQueueJob();
        $past_job->user = $data['user'];
        if (array_key_exists('id', $data)) {
            $page = Page::find($data['id']);
            $past_job->type = "Page with title: $page->title";
        } else {
            $past_job->type = 'ALL';
        }
        $past_job->save();
    }

    public function getRedirectHtml($page)
    {
        $platform = $page->platform;
        $locales = array_keys(Locale::listEnabled());
        $locales_str = '';
        foreach ($locales as $locale) {
            $locales_str .= "'$locale',";
        }

        $locale_page_slugs = [];
        foreach ($locales as $locale) {
            $page->noFallbackLocale()->lang($locale);
            if (empty($page->slug)) {
                continue;
            }
            $path = $page->slug === '/' ? "/$locale" : "/$locale$page->slug";
            $locale_page_slugs[$locale] = $path;
        }

        $pages_str = '{';
        foreach ($locale_page_slugs as $index => $slug) {
            $domain = $platform->domain;
            $pages_str .= "$index: 'https://$domain$slug',";
        }
        $pages_str .= '}';


        $html = "
        <!DOCTYPE html>
            <html lang=\"en\">
            <head>
              <meta charset=\"utf-8\">
              <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
              <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">

              <title></title>
            <script>
                var languages = [$locales_str];
                var pages = {$pages_str};
                var language = window.navigator.userLanguage || window.navigator.language;
                var langCode = language.split(\"-\")[0];

                if(!(langCode in pages)) {
                    langCode = 'en';
                }

                location.href = pages[langCode] + document.location.search;
            </script>

            </head>
            <body>

            </body>
            </html>
        ";

        foreach ($locales as $locale) {
            $page->noFallbackLocale()->lang($locale);
            if (empty($page->slug)) {
                continue;
            }
            $this->storeRedirect($html, $page->slug, $platform);
        }
    }


    public function uploadErrorPage()
    {
        foreach (Platform::all() as $platform) {
            $this->errorPage($platform);
        }
    }

    public function errorPage($platform)
    {
        $locales = array_keys(Locale::listEnabled());
        $locales_str = '';
        $pages_str = '{';
        foreach ($locales as $locale) {
            $locales_str .= "'$locale',";
            $pages_str .= "$locale: 'https://$platform->domain/$locale/error',";
            $request = Request::create("https://$platform->domain/getErrorPage", 'GET');
            $controller = new Pages();
            $content = $controller->pageNotFound($request, $locale, $platform->domain);
            $this->uploadToS3($content, "/$locale/error", $platform);
        }

        $pages_str .= '}';


        $html = "
        <!DOCTYPE html>
            <html lang=\"en\">
            <head>
              <meta charset=\"utf-8\">
              <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
              <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">

              <title></title>
            <script>
                var languages = [$locales_str];
                var pages = {$pages_str};
                var language = window.navigator.userLanguage || window.navigator.language;
                var langCode = language.split(\"-\")[0];

                if(!(langCode in pages)) {
                    langCode = 'en';
                }

                location.href = pages[langCode];
            </script>

            </head>
            <body>

            </body>
            </html>
        ";

        $this->storeRedirect($html, "/error", $platform);
    }

    public function uploadSitemap()
    {
        $locales = array_keys(Locale::listEnabled());
        foreach (Platform::all() as $platform) {
            foreach ($locales as $locale) {
                $sitemap = $this->renderSitemap($platform, $locale);
                $this->storeSitemap($sitemap['content'], "/$locale/sitemap.xml", $platform);
            }
            $index_sitemap = $this->renderIndex($platform);
            $this->storeSitemap($index_sitemap['content'], "/sitemap.xml", $platform);
        }
    }

    public function renderIndex($platform)
    {
        $sitemap = App::make("sitemap");
        $sitemap->model->setSloc($platform->url.'/app/css/');

        $locales = array_keys(Locale::listEnabled());

        foreach ($locales as $locale) {
            $sitemap->addSitemap("$platform->url/$locale/sitemap.xml");
        }

        return $sitemap->generate('sitemapindex');
    }

    public function renderSitemap($platform, $lang)
    {
        $sitemap = App::make("sitemap");
        $sitemap->model->setSloc($platform->url.'/app/css/');

        $pages = Page::where('platform_id', $platform->id)->get();
        $locales = array_keys(Locale::listEnabled());

        foreach ($pages as $page) {
            if ($page->seo_tag !== null && $page->seo_tag->robot_index === 'noindex') {
                continue;
            }
            if (($version = $page->currentModel($lang)) && !empty($version->slug) && $version->published) {
                $translations = [];
                foreach ($locales as $locale) {
                    if ($locale === $lang) {
                        continue;
                    }
                    if (($locale_version = $page->currentModel($locale)) && !empty($locale_version->slug)) {
                        $translations[] = [
                            'language' => $locale,
                            'url' => "$platform->url/$locale$locale_version->slug"
                        ];
                    }
                }
                $path = $version->slug === '/' ? '' : $version->slug;
                $sitemap->add("$platform->url/$lang$path", null, '1.0', 'daily', [], null, $translations);
            }
        }

        return $sitemap->generate('xml');
    }

    public function uploadRobots()
    {
        foreach (Platform::all() as $platform) {
            $content = Robots::get();
            $content .= "\nSitemap: $platform->url/sitemap.xml";
            $this->storeRobots($content, '/robots.txt', $platform);
        }
    }
}
