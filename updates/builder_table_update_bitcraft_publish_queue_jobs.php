<?php namespace Bitcraft\Publish\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPublishQueueJobs extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_publish_queue_jobs', function($table)
        {
            $table->integer('settings_id')->unsigned()->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_publish_queue_jobs', function($table)
        {
            $table->dropColumn('settings_id');
        });
    }
}
