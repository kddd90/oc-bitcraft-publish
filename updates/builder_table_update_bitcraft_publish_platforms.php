<?php namespace Bitcraft\Publish\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBitcraftPublishPlatforms extends Migration
{
    public function up()
    {
        Schema::table('bitcraft_publish_platforms', function($table)
        {
            $table->integer('settings_id')->nullable(false)->default(1)->change();
        });
    }
    
    public function down()
    {
        Schema::table('bitcraft_publish_platforms', function($table)
        {
            $table->integer('settings_id')->nullable()->default(null)->change();
        });
    }
}
