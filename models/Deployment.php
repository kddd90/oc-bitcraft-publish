<?php namespace Bitcraft\Publish\Models;

use Model;

/**
 * Model
 */
class Deployment extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_publish_deployments';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'jobs' => ['Bitcraft\Publish\Models\QueueJob', 'key' => 'settings_id',],
        'past_jobs' => ['Bitcraft\Publish\Models\PastQueueJob', 'key' => 'settings_id',]
    ];
}
