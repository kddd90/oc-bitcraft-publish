<?php namespace Bitcraft\Publish\Models;

use Model;

/**
 * Model
 */
class PastQueueJob extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_publish_past_queue_jobs';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
