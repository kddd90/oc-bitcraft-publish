<?php namespace Bitcraft\Publish\Behaviors;

use Backend\Classes\ControllerBehavior;
use Backend\Facades\BackendAuth;
use Bitcraft\Versions\Models\Version;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use RainLab\Translate\Models\Locale;

class PublishControllerBehavior extends ControllerBehavior
{
    protected $parent;

    protected $extended = false;

    public function __construct($controller)
    {
        parent::__construct($controller);

        $controller::extendFormFields(function ($form, $model, $context) {
            if ($form->getContext() != 'update' || $this->extended) {
                return;
            }
            $this->extended = true;

            $form->addTabFields([
                'publish_to_s3' => [
                    'label' => '',
                    'span' => 'full',
                    'tab' => 'Publish to S3',
                    'type'  => 'partial',
                    'path' => '~/plugins/bitcraft/publish/partials/publish_to_s3.htm'
                ]
            ]);
        });
    }

    public function onUpload($id)
    {
        $model = post('model');
        if ($page = $model::find($id)) {
            $page->renderPage();
            \Flash::success('Page will be uploaded to S3');
            return back();
        } else {
            \Flash::error('Error!');
            return back();
        }
    }
}
